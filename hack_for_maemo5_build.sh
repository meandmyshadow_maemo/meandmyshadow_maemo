#!/bin/sh
#because SDL-mixer is so crappy, compile mixer inside binary
#This script is run between cmake and make. It will tune linking settings.


mv CMakeFiles/meandmyshadow.dir/link.txt .
#change -DMAEMO5 to line break
sed -i 's/-DMAEMO5/\n/g' link.txt
#take first row
head -n 1 link.txt > temp
#add correct content
echo -n " -DMAEMO5 `pkg-config --libs --cflags  vorbisfile ` " >> temp
#add last (second) row
tail -n 1 link.txt  >> temp
#remove line breaks 
while read line; do echo -n "$line "; done < temp > link.txt
#copy it back
mv link.txt CMakeFiles/meandmyshadow.dir/link.txt


exit 0
